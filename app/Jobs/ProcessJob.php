<?php

namespace App\Jobs;

use App\Job;
use App\JobAccount;
use App\MailingList;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class ProcessJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected  $jobData;
    /**
     * Create a new job instance.
     *
     * @param  Job $job Job object
     * @return void
     */
    public function __construct(Job $job)
    {
        $this->connection = 'rabbitmq';
        $this->jobData = $job;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 300);
        $this->jobData->update(['status' => 'processing']);

        $segment = $this->jobData->mailingList->segment;
        $conditions = [];
        if ($segment->country_id) {
            $conditions['country_id'] = $segment->country_id;
        }
        if ($segment->from_age) {
            $conditions['from_age'] = $segment->from_age;
        }
        if ($segment->to_age) {
            $conditions['to_age'] = $segment->to_age;
        }
        if ($segment->gender) {
            $conditions['gender'] = $segment->gender;
        } else {
            $conditions['gender'] = 'both';
        }

        $start = microtime(true);
        DB::statement(
            DB::raw("call populateJobAccounts(:job_id, :conditions)"),
            [
                'job_id' => $this->jobData->id,
                'conditions' => json_encode($conditions)
            ]
        );
        $time = microtime(true) - $start;
        Log::info('job done in ' . $time . ' seconds');

        // Start creating jobs in queue emails 
        $start = microtime(true);
        $jobAccounts = JobAccount::where('job_id', '=', $this->jobData->id)
            ->select('email', 'id')->get()->toArray();
        
        $number_of_accounts = count($jobAccounts);
        if ($number_of_accounts < 1000) {
            $limit = 100;
        } elseif ($number_of_accounts >= 1000 && $number_of_accounts < 50000) {
            $limit = $number_of_accounts/10;
        } elseif ($number_of_accounts < 500000) {
            $limit = $number_of_accounts/50;
        } elseif ($number_of_accounts < 1000000) {
            $limit = $number_of_accounts/100;
        } else {
            $limit = 10000;    
        }

        foreach (array_chunk($jobAccounts, $limit) as $accounts ) {
            ProcessEmails::dispatch($this->jobData->id, $accounts)
                ->onQueue('emails');
        }
        $time = microtime(true) - $start;
        Log::info('accounts queued in' . $time . ' seconds');
    }

    public function failed(\Exception $e)
    {
        Log::debug($e);
    }
}
