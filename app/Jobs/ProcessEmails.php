<?php

namespace App\Jobs;

use App\Account;
use App\Job;
use App\JobAccount;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Mail;
use App\Mail\WelcomeEmail;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use mysql_xdevapi\Exception;

class ProcessEmails implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $tries = 5;

    protected $accounts;

    protected $jobData;

    protected $email_body;

    protected $requeue;

    /**
     * Create a new job instance.
     *
     * @param  int   $jobId    Job id
     * @param  array $accounts Accounts to be queued
     * @return void
     */
    public function __construct($jobId , $accounts)
    {
        $this->connection = 'rabbitmq';

        $this->accounts = $accounts;
        $this->jobData = Job::find($jobId);
        $this->email_body = $this->jobData->mailingList->template;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $start = microtime(true);
        $success = [];
        foreach ($this->accounts as $key => $email) {
            try {
                Mail::send(
                    [], [], function ($message) use ($email) {
                        $message->to($email['email'])
                            ->subject('Mail from our awesome website!')
                            ->setBody($this->email_body);
                    }
                );

                if (count(Mail::failures()) > 0 ) {
                    // if no exception thrown but we have some error
                    // means transport didn't accept email
                    throw  new \Exception('Transport didnt accept email');
                }
                $success[] = $email['id'];
            } catch (\Exception $exception) {
                $failedJob = JobAccount::find($email['id']);
                Log::info('Error ' . $exception->getMessage());

                //Dont requeue same account if failed second time
                if (is_null($failedJob->status)) {
                    $failedJob->update(['status' => "0"]);
                    $this->requeue[] = $email;
                } else {
                    Log::info('Error ' . $exception->getMessage());
                }
            }
        }

        // update items status to complete
        JobAccount::whereIn('id', $success)
            ->update(['status' => "1"]);

        if (!$this->jobData->jobAccounts()->whereNull('status')->limit(1)->count()) {
            //Job finished , update status
            $this->jobData->update(['status' => 'complete']);
        }

        $time = microtime(true) - $start;
        Log::info('mails sent in' . $time . ' seconds');

        //if we have failed emails try once more
        if ($this->requeue) {
            Log::info('requeue started');
            $this->dispatch($this->jobData->id, $this->requeue)
                ->onConnection($this->connection)
                ->onQueue($this->queue)
                ->delay(20);
        }
    }
}
