<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobAccount extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'job_id',
        'account_id',
        'email',
        'status'
    ];

    /**
     * Get Acount for JobAccounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function account()
    {
        return $this->belongsTo('App\Account');
    }
}
