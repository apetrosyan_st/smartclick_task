<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Country;
use App\Segment;
use App\MailingList;

class MailingListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $mailing_lists = MailingList::with('segment')
            ->orderBy('id', 'desc')
            ->paginate(20);
        return view('mailing_lists.index', compact('mailing_lists'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $segments = Segment::pluck('name', 'id');
        return view('mailing_lists.create', compact('segments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'name'=>'required',
            'segment_id'=>'required|exists:segments,id',
            'template'=>'required',
            ]
        );

        $mailing_list = new MailingList(
            [
            'name' => $request->get('name'),
            'segment_id' => $request->get('segment_id'),
            'template' => $request->get('template')
            ]
        );
        $mailing_list->save();
        return redirect('/mailing_lists')->with('success', 'Mailing list saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id Mailing list id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mailing_list = MailingList::findOrFail($id);
        $segments = Segment::pluck('name', 'id');
        return view('mailing_lists.edit', compact('segments', 'mailing_list'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @param  int                      $id      Mailing list id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'name'=>'required',
            'segment_id'=>'required|exists:segments,id',
            'template'=>'required',
            ]
        );

        $mailing_list = MailingList::findOrFail($id);
        $mailing_list->name = $request->get('name');
        $mailing_list->segment_id = $request->get('segment_id');
        $mailing_list->template = $request->get('template');
        $mailing_list->save();

        return redirect('/mailing_lists')->with('success', 'Mailing List updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id Mailing List id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $mailing_list = MailingList::findOrFail($id);
        $mailing_list->delete();

        return redirect('/mailing_lists')->with('success', 'Segment deleted!');
    }
}
