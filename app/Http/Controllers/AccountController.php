<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;
use App\Account;

class AccountController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $accounts = Account::with('country')->orderBy('id', 'desc')->paginate(50);
        return view('accounts.index', compact('accounts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name', 'id');
        return view('accounts.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate(
            [
            'email'=>'bail|required|email|unique:accounts',
            'age'=>'required',
            'gender'=>'required',
            'country_id'=>'required',
            ]
        );

        $account = new Account(
            [
            'email' => $request->get('email'),
            'age' => $request->get('age'),
            'gender' => $request->get('gender'),
            'country_id' => $request->get('country_id')
            ]
        );
        $account->save();
        return redirect('/accounts')->with('success', 'account saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id Account id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $account = Account::findOrFail($id);
        $countries = Country::pluck('name', 'id');
        return view('accounts.edit', compact('account', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @param  int                      $id      Account id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate(
            [
            'email'=>'required',
            'age'=>'required',
            'gender'=>'required',
            'country_id'=>'required',
            ]
        );


        $account = Account::findOrFail($id);
        $account->email = $request->get('email');
        $account->age = $request->get('age');
        $account->gender = $request->get('gender');
        $account->country_id = $request->get('country_id');
        $account->save();

        return redirect('/accounts')->with('success', 'Account updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id Account id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $account = account::findOrFail($id);
        $account->delete();

        return redirect('/accounts')->with('success', 'Account deleted!');
    }
}
