<?php

namespace App\Http\Controllers;

use App\Jobs\ProcessEmails;
use App\Account;
use App\Job;
use App\Jobs\ProcessJob;
use App\MailingList;
use App\Segment;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jobs = Job::with('mailingList.segment')
            ->orderBy('id', 'desc')
            ->paginate(20);
        return view('jobs.index', compact('jobs'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id Job Id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $job = Job::findOrFail($id);
        $failed_accounts = $job->jobAccounts()
            ->where('status', '=', 0)
            ->paginate(20);

        $diff = Carbon::parse($job->created_at)
            ->diffInSeconds(Carbon::parse($job->updated_at));


        $status = [
            'all' =>  $job->jobAccounts()->count('id'),
            'done' => $job->jobAccounts()->where('status', '=', 1)->count('id'),
            'failed' => $job->jobAccounts()->where('status', '=', 0)->count('id'),
            'diff' => $diff
        ];
        $status['percent'] = ($status['all'])?(($status['done'] + $status['failed'])*100)/$status['all']:0;
        return view('jobs.show', compact('job', 'failed_accounts', 'status'));
    }

    /**
     * Function to add job for particular Mailing list
     *
     * @param  int $id Mailing LIst Id to add job for
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function processQueue($id)
    {
        $mailing_list = MailingList::findOrFail($id);

        $job = new Job(
            [
            'mailing_list_id' => $mailing_list->id
            ]
        );
        $job->save();
        ProcessJob::dispatch($job)->onQueue('jobs');
        return redirect('/jobs')->with('success', 'Job created');
    }

    /**
     * Function to get current status of job
     *
     * @param  int $id Job id
     * @return array
     */
    public function checkStatus($id)
    {
        $job = Job::findOrFail($id);
        return [
            'all' =>  $job->jobAccounts()->count(),
            'done' => $job->jobAccounts()->where('status', '=', 1)->count(),
            'failed' => $job->jobAccounts()->where('status', '=', 0)->count(),
        ];
    }
}