<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;
use App\Country;
use App\Segment;

class SegmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $segments = Segment::with('country')->orderBy('id', 'desc')->paginate(50);
        return view('segments.index', compact('segments'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::pluck('name', 'id');
        return view('segments.create', compact('countries'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $messages = [
            'gt' => 'Ages to must be grater than Ages from',
        ];
        $request->validate(
            [
            'name'=>'required',
            'from_age'=>'nullable|integer|max:100|min:10',
            'to_age'=>'nullable|gt:from_age|integer|max:100|min:10',
            'gender'=>'nullable|in:Male,Female',
            'country_id'=>'nullable|exists:countries,id',
            ], $messages
        );

        $segment = new Segment(
            [
            'name' => $request->get('name'),
            'from_age' => ($request->get('from_age'))?$request->get('from_age'):null,
            'to_age' => ($request->get('to_age'))?$request->get('to_age'):null,
            'gender' => ($request->get('gender'))?$request->get('gender'):null,
            'country_id' => ($request->get('country_id'))?$request->get('country_id'):null
            ]
        );
        $segment->save();
        return redirect('/segments')->with('success', 'segment saved!');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id Segment id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $segment = Segment::findOrFail($id);
        $countries = Country::pluck('name', 'id');
        return view('segments.edit', compact('segment', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request Request received
     * @param  int                      $id      Segment id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $messages = [
            'gt' => 'Ages to must be grater than Ages from',
        ];
        $request->validate(
            [
            'name'=>'required',
            'from_age'=>'present|integer|max:100|min:10',
            'to_age'=>'present|gt:from_age|integer|max:100|min:10',
            'gender'=>'nullable|in:Male,Female',
            'country_id'=>'present|exists:countries,id',
            ], $messages
        );


        $segment = Segment::findOrFail($id);
        $segment->name = $request->get('name');
        $segment->from_age = ($request->get('from_age'))?$request->get('from_age'):null;
        $segment->to_age = ($request->get('to_age'))?$request->get('to_age'):null;
        $segment->gender = ($request->get('gender'))?$request->get('gender'):null;
        $segment->country_id = ($request->get('country_id'))?$request->get('country_id'):null;
        $segment->save();

        return redirect('/segments')->with('success', 'segment updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id Segment id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $segment = Segment::findOrFail($id);
        $segment->delete();

        return redirect('/segments')->with('success', 'Segment deleted!');
    }

    /**
     * List of accounts that matched for segment.
     *
     * @param  int $id Segment id
     * @return \Illuminate\Http\Response
     */
    public function matched($id)
    {
        $segment = Segment::findOrFail($id);
        $accounts = Account::with('country')
        ->when(
            $segment->gender, function ($query) use ($segment) {
                $query->where('gender', '=', $segment->gender);
            }
        )
        ->when(
            $segment->from_age, function ($query) use ($segment) {
                $query->where('age', '>', $segment->from_age);
            }
        )
        ->when(
            $segment->to_age, function ($query) use ($segment) {
                $query->where('age', '<', $segment->to_age);
            }
        )
        ->when(
            $segment->country_id, function ($query) use ($segment) {
                $query->where('country_id', '=', $segment->country_id);
            }
        )
        ->paginate(20);

        return view('segments.matched', compact('segment', 'accounts'));
    }
}
