<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MailingList extends Model
{
    protected $fillable = [
        'name',
        'segment_id',
        'processed',
        'template'
    ];

    /**
     * Get the Mailing lists segment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function segment()
    {
        return $this->belongsTo('App\Segment');
    }
}
