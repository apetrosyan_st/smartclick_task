<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $fillable = [
        'name',
        'mailing_list_id',
        'status'
    ];

    /**
     * Get job MailingList
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function mailingList()
    {
        return $this->belongsTo('App\MailingList');
    }

    /**
     * Get Jobs JobAccounts
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function jobAccounts()
    {
        return $this->hasMany('App\JobAccount');
    }
}
