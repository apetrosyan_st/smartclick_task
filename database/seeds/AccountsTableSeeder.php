<?php
use Crockett\CsvSeeder\CsvSeeder;

class AccountsTableSeeder extends CsvSeeder {


    public function __construct()
    {
        $this->table = 'accounts';
        #$this->insert_chunk_size = 1000;
        #$this->filename = base_path().'/database/seeds/csvs/accounts.csv';
    }

    public function run()
    {
        // Recommended when importing larger CSVs
        DB::disableQueryLog();

        // Uncomment the below to wipe the table clean before populating
        DB::table($this->table)->truncate();
        $i = 200;
        while ($i){
            $this->seedFromCSV(base_path('database/seeds/csvs/accounts.csv'), 'accounts');
            $i--;
        }
    }
}
