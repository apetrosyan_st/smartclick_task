<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;

use Illuminate\Database\Migrations\Migration;

class CreateAfterJobsInsertTrigger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
            CREATE TRIGGER after_jobs_insert
                AFTER INSERT
                on jobs
                for each row
                    begin
                update mailing_lists set processed = processed+1 where id=NEW.mailing_list_id ;
            END;
        ";

        DB::unprepared("DROP trigger IF EXISTS after_jobs_insert");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP trigger IF EXISTS after_jobs_insert");
    }
}
