<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePopulateJobAccountsProcedure extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $procedure = "
            CREATE PROCEDURE `populateJobAccounts`(IN jobId bigint, IN conditions json)
            BEGIN
                DECLARE countryId INT DEFAULT null;
                DECLARE fromAge INT DEFAULT null;
                DECLARE toAge INT DEFAULT null;
                DECLARE genderVar VARCHAR(20) DEFAULT null;
                SET  countryId = IFNULL(json_extract(conditions, '$.country_id'),0);
                SET  fromAge = IFNULL(json_extract(conditions, '$.from_age'),0);
                SET  toAge = IFNULL(json_extract(conditions, '$.to_age'),0);
                
                INSERT INTO job_accounts (account_id, email, job_id)
                    SELECT `id` as `account_id`, email, jobId as job_id FROM accounts
                WHERE
                    (country_id = countryId OR countryId = 0)
                    AND ( age > fromAge OR  fromAge = 0)
                    AND ( age < toAge OR  toAge = 0)
                    AND ( gender = json_extract(conditions, '$.gender') OR  json_extract(conditions, '$.gender') = 'both')
            END
        ";

        DB::unprepared("DROP procedure IF EXISTS populateJobAccounts");
        DB::unprepared($procedure);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::unprepared("DROP procedure IF EXISTS populateJobAccounts");
    }
}
