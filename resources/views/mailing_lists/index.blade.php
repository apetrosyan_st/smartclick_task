@extends('layouts.app')
@section('title', 'Mailing lists')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h1 class="display-3">Mailing lists</h1>
                <a href="{{ route('mailing_lists.create') }}" class="btn btn-primary-outline btn-link float-right">Add Mailing List</a>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Segment</td>
                        <td>Processed</td>
                        <td colspan=3>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($mailing_lists as $mailing_list)
                        <tr>
                            <td>{{$mailing_list->id}}</td>
                            <td>{{$mailing_list->name}}</td>
                            <td>{{($mailing_list->segment) ? $mailing_list->segment->name : ''}}</td>
                            <td>{{$mailing_list->processed}}</td>
                            <td>
                                <a href="{{ route('mailing_lists.edit',$mailing_list->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <a href="{{ route('mailing_lists.process',$mailing_list->id)}}" class="btn btn-primary">Add Job for sending emails</a>
                            </td>
                            <td>
                                <form action="{{ route('mailing_lists.destroy', $mailing_list->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $mailing_lists->links() }}
            </div>
        <div>
    </div>
@endsection
