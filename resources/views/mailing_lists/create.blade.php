@extends('layouts.app')

@section('title', 'Add Mailing List')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-sm-auto offset-sm-1">
                    <h1 class="display-3">Add Mailing List</h1>
                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        {{ Form::open(['url' => route('mailing_lists.store')]) }}
                            @csrf
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('template', 'Template:') }}
                                {{ Form::textarea('template', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('segment_id', 'Segment:') }}
                                {{ Form::select('segment_id', $segments, null, ['class' => 'form-control', 'placeholder' => 'Select Segment']) }}
                            </div>
                            <a href="{{ route('mailing_lists.index') }}" class="btn btn-primary-outline btn-link">Cancel</a>
                            <button type="submit" class="btn btn-primary-outline">Add mailing list</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection