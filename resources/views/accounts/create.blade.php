@extends('layouts.app')

@section('title', 'Add Account')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-sm-auto offset-sm-1">
                    <h1 class="display-3">Add a account</h1>
                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        {{ Form::open(['url' => route('accounts.store')]) }}
                            @csrf
                            <div class="form-group">
                                {{ Form::label('email', 'Email:') }}
                                {{ Form::email('email', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('age', 'Age:') }}
                                {{ Form::number('age', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('gender', 'Male') }}
                                {{ Form::radio('gender', 'Male') }}
                                {{ Form::label('gender', 'Female') }}
                                {{ Form::radio('gender', 'Female') }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('country_id', 'Country:') }}
                                {{ Form::select('country_id', $countries, null, ['class' => 'form-control', 'placeholder' => 'Select Country']) }}
                            </div>
                            <a href="{{ route('accounts.index') }}" class="btn btn-primary-outline btn-link">Cancel</a>
                            <button type="submit" class="btn btn-primary-outline">Add account</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection