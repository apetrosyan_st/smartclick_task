@extends('layouts.app')

@section('title', 'Edit Account')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-sm-auto offset-sm-1">
                    <h1 class="display-3">Edit a account</h1>
                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        {{ Form::open(['url' => route('accounts.update', $account->id), 'method' => 'put']) }}
                        @csrf
                        <div class="form-group">
                            {{ Form::label('email', 'Email:') }}
                            {{ Form::email('email', $account->email, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('age', 'Age:') }}
                            {{ Form::number('age', $account->age, ['class' => 'form-control']) }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('gender', 'Male') }}
                            {{ Form::radio('gender', 'Male', $account->gender == 'Male') }}
                            {{ Form::label('gender', 'Female') }}
                            {{ Form::radio('gender', 'Female', $account->gender == 'Female') }}
                        </div>

                        <div class="form-group">
                            {{ Form::label('country_id', 'Country:') }}
                            {{ Form::select('country_id', $countries, $account->country_id, ['class' => 'form-control', 'placeholder' => 'Select Country']) }}
                        </div>
                        <a href="{{ route('accounts.index') }}" class="btn btn-primary-outline btn-link">Cancel</a>
                        <button type="submit" class="btn btn-primary-outline">Update account</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection