@extends('layouts.app')
@section('title', 'Job Details')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h1 class="display-3">Job status for {{ $job->mailingList->name }}</h1>
                <ul>
                    <li>Status :
                        @if ($job->status == 'processing')
                            {{ $status['percent'] . '% done' }}
                        @else
                            {{$job->status}}
                        @endif
                    </li>
                    <li>Overall : {{ $status['all'] }}</li>
                    <li>Processed : {{ $status['done'] + $status['failed']}}</li>
                    <li>Successful : {{ $status['done'] }}</li>
                    <li>Failed : {{ $status['failed'] }}</li>
                    @if ($job->status == 'complete')
                        <li>Duration : {{ $status['diff']}} seconds</li>
                    @endif
                </ul>

                <h4 class="display-6">List of accounts, wich faild to send email</h4>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Email</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($failed_accounts as $account)
                        <tr>
                            <td>{{$account->id}}</td>
                            <td>{{$account->email}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $failed_accounts->links() }}
            </div>
        <div>
    </div>
@endsection
