@extends('layouts.app')
@section('title', 'Jobs')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h1 class="display-3">Jobs</h1>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Mailing List</td>
                        <td>Segment</td>
                        <td>Status</td>
                        <td>View</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($jobs as $job)
                        <tr>
                            <td>{{$job->id}}</td>
                            <td>{{($job->mailingList) ? $job->mailingList->name : ''}}</td>
                            <td>{{($job->mailingList && $job->mailingList->segment) ? $job->mailingList->segment->name : ''}}</td>
                            <td>{{$job->status}}</td>
                            <td>
                                <a href="{{ route('jobs.show',$job->id)}}" class="btn btn-primary">View</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $jobs->links() }}
            </div>
        <div>
    </div>
@endsection
