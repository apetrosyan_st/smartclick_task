@extends('layouts.app')
@section('title', 'Segments')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h1 class="display-3">Segments</h1>
                <a href="{{ route('segments.create') }}" class="btn btn-primary-outline btn-link float-right">Add Segment</a>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Name</td>
                        <td>Age</td>
                        <td>Gender</td>
                        <td>Country</td>
                        <td colspan = 4>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($segments as $segment)
                        <tr>
                            <td>{{$segment->id}}</td>
                            <td>{{$segment->name}}</td>
                            <td>{{$segment->from_age . ' - ' . $segment->to_age}}</td>
                            <td>{{$segment->gender}}</td>
                            <td>{{($segment->country) ? $segment->country->name : ''}}</td>
                            <td>
                                <a href="{{ route('segments.edit',$segment->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <a href="{{ route('segments.matched',$segment->id)}}" class="btn btn-primary">View Matched</a>
                            </td>
                            <td>
                                <form action="{{ route('segments.destroy', $segment->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $segments->links() }}
            </div>
        <div>
    </div>
@endsection
