@extends('layouts.app')
@section('title', 'Accounts')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                @if(session()->get('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                <h1 class="display-3">Matched Accounts for {{ $segment->name }}</h1>
                <ul>
                    <li>Gender : {{ $segment->gender }}</li>
                    <li>Age : {{ $segment->from_age . '-' . $segment->to_age }}</li>
                    <li>Country : {{($segment->country) ? $segment->country->name : ''}}</li>
                    <li>Total number of matched accounts: {{$accounts->total()}}</li>

                </ul>
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Email</td>
                        <td>Age</td>
                        <td>Gender</td>
                        <td>Country</td>
                        <td colspan = 2>Actions</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($accounts as $account)
                        <tr>
                            <td>{{$account->id}}</td>
                            <td>{{$account->email}}</td>
                            <td>{{$account->age}}</td>
                            <td>{{$account->gender}}</td>
                            <td>{{$account->country->name}}</td>
                            <td>
                                <a href="{{ route('accounts.edit',$account->id)}}" class="btn btn-primary">Edit</a>
                            </td>
                            <td>
                                <form action="{{ route('accounts.destroy', $account->id)}}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-danger" type="submit">Delete</button>
                                </form>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                {{ $accounts->links() }}
            </div>
        <div>
    </div>
@endsection
