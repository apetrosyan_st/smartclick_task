@extends('layouts.app')

@section('title', 'Add Segment')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="row">
                <div class="col-sm-auto offset-sm-1">
                    <h1 class="display-3">Add a segment</h1>
                    <div>
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div><br />
                        @endif
                        {{ Form::open(['url' => route('segments.store')]) }}
                            @csrf
                            <div class="form-group">
                                {{ Form::label('name', 'Name:') }}
                                {{ Form::text('name', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('from_age', 'Ages from:') }}
                                {{ Form::number('from_age', null, ['class' => 'form-control']) }}
                            </div>
                            <div class="form-group">
                                {{ Form::label('to_age', 'Ages before:') }}
                                {{ Form::number('to_age', null, ['class' => 'form-control']) }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('gender', 'Male') }}
                                {{ Form::radio('gender', 'Male') }}
                                {{ Form::label('gender', 'Female') }}
                                {{ Form::radio('gender', 'Female') }}
                            </div>

                            <div class="form-group">
                                {{ Form::label('country_id', 'Country:') }}
                                {{ Form::select('country_id', $countries, null, ['class' => 'form-control', 'placeholder' => 'Select Country']) }}
                            </div>
                            <a href="{{ route('segments.index') }}" class="btn btn-primary-outline btn-link">Cancel</a>
                            <button type="submit" class="btn btn-primary-outline">Add segment</button>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection