<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index');
Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('accounts', 'AccountController');

    Route::resource('segments', 'SegmentController');
    Route::get('/segments/{id}/matched', 'SegmentController@matched')->name('segments.matched');

    Route::resource('mailing_lists', 'MailingListController');
    Route::get('/mailing_lists/{id}/process', 'JobController@processQueue')->name('mailing_lists.process');

    Route::get('/jobs', 'JobController@index')->name('jobs.index');
    Route::get('/jobs/{id}', 'JobController@show')->name('jobs.show');
    Route::get('/jobs/{id}/status', 'JobController@checkStatus')->name('jobs.status');
    Route::get('/test', 'JobController@test')->name('jobs.status');
});